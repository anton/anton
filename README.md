<h1 align="center">
    Hello world, I am Anton Smith!
    <a href="https://gitlab.com/anton" target="_self">
        <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="30">
    </a>
</h1>
<br/>
<p align="center">
    <a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.herokuapp.com?font=Fira+Code&pause=1000&random=false&width=435&lines=I+am+a+Staff+Support+Engineer;I+work+at+GitLab;I+live+in+Dunedin,+New+Zealand" alt="Typing SVG" /></a>
</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/sWOUi0PVTXw?si=q4tTIMXsB5BK6hP6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Welcome! This page is designed to help others understand what it's like to work with me, especially if we haven't worked together before. I'm a passionate and collaborative person, and I believe in fostering a supportive and productive environment for everyone I work with.

I welcome any feedback or suggestions to improve this page!

## About me
I live in [Dunedin, New Zealand](https://en.wikipedia.org/wiki/Dunedin), where I enjoy a mix of creative activities and professional growth. My background spans various fields including retail and information technology, but I've always gravitated toward roles where I can solve problems and help people.

I believe in being transparent, open, and supportive, and I'm always learning and evolving both professionally and personally.

## My Approach to Work

### Working Together

- **Communication**: I'm straightforward and appreciate direct communication. If there's something on your mind, please feel free to share. I value open, honest dialogue and am always open to constructive feedback.
- **Respect and Dignity**: I deeply respect your time, energy, and individuality. I consider it a privilege to work with you.
- **Well-Being**: Life doesn't stop at work, and I encourage open discussions if something is impacting your work life. I'm a believer in the importance of mental health, and it's okay to bring your whole self to work.
- **Emotion and Empathy**: I understand that emotions are a natural part of work and life. If you ever need support or someone to talk to, I'm happy to listen.

### What I Expect from You

- **Honesty and Integrity**: I trust you to communicate openly and honestly. We're all human, and I expect you to take accountability for your actions as I will mine.
- **Desire to Contribute**: I look for team members who bring a genuine desire to make an impact, contribute to our mission, and support each other.
- **Positive Intent**: I assume the best in people and will always approach situations with empathy and understanding.

## Communication Preferences

- **Work Hours**: I typically work from 9:00 AM to 05:00 PM [NZST/NZDT (UTC+12/UTC+13)](https://www.timeanddate.com/time/zone/new-zealand/auckland). During this time, I am available for meetings and task work, though I may be away from my desk occasionally.
- **Response Time**: I strive to be timely with responses, but please feel free to flag anything urgent so I can prioritize it accordingly.
- **Asynchronous Communication**: I prefer asynchronous communication whenever possible, as it helps me manage my workload efficiently. If something needs my immediate attention, feel free to reach out directly.
- **Avoid Direct Messages**: [Please avoid direct messages when possible](https://handbook.gitlab.com/handbook/communication/#avoid-direct-messages), as it helps ensure that others can assist you in case I am unavailable. Direct messages are best reserved for urgent matters that need immediate attention.
- **Scheduling Events**: You're welcome to schedule pairing sessions and meetings directly in my calendar without prior approval. I am here to help the team succeed where I can.
- **Boundaries & Availability**: While I am happy to support the team, I also value maintaining a healthy work/life balance. I respect both my time and yours, so please only contact me after hours if it's urgent.

## WFH Setup

Here’s a look at the equipment I use for my work-from-home setup:

| Item       | Brand  |
| ---------- | ------ |
| Web Camera | [Logitech Brio 4K](https://www.logitech.com/en-nz/products/webcams/brio-4k-hdr-webcam.html)  |
| Microphone | [Shure MV7+](https://www.shure.com/en-ASIA/products/microphones/mv7) |
| Monitor    | [Dell 49' Ultrawide USB-C Monitor](https://www.dell.com/en-nz/shop/dell-ultrasharp-49-curved-usb-c-hub-monitor-u4924dw/apd/210-bgvm/monitors-monitor-accessories) |
| Headphones | [Sony WH-1000XM4 Noise Cancelling Headphones](https://store.sony.co.nz/headphones-noisecancelling/WH1000XM4B.html) |
| Keyboard   | [Keychron K14 Pro](https://www.keychron.com/products/keychron-k14-pro-qmk-via-wireless-mechanical-keyboard) |
| Mouse      | [Logitech MX Master 3S](https://www.logitech.com/en-us/products/mice/mx-master-3s.html) |
| Chair      | [Secretlab Titan EVO](https://secretlab.co.nz/products/titan-evo-2022-series)  |
| Desk       | [Evolve Electric Standing Desk 1800x700](https://unofurniture.co.nz/products/evolve-electric-standing-desk?variant=29081232441459) |
